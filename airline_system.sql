-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2023 at 12:08 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airline_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `airline_info`
--

CREATE TABLE `airline_info` (
  `Airline_id` int(11) NOT NULL,
  `airline_name` varchar(255) DEFAULT NULL,
  `airline_info` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `airplane`
--

CREATE TABLE `airplane` (
  `plane_code` int(11) NOT NULL,
  `airline_company` varchar(255) DEFAULT NULL,
  `airline_Id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `User_id` int(11) DEFAULT NULL,
  `Customer_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  `passport_number` int(11) DEFAULT NULL,
  `passport_expiration_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flight`
--

CREATE TABLE `flight` (
  `Flight_code` varchar(255) NOT NULL,
  `Airline_company` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `terminal` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `gate` int(11) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `plane_code` int(255) DEFAULT NULL,
  `plane_availability` varchar(255) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `flight`
--

INSERT INTO `flight` (`Flight_code`, `Airline_company`, `created_by`, `terminal`, `remarks`, `origin`, `gate`, `destination`, `plane_code`, `plane_availability`, `Price`) VALUES
('101', 'PAL', 'John', 2, 'Paid', 'Manila Only', 2, 'cebu', 200, 'available', 2300),
('102', 'PAL', 'Anthony', 2, 'Paid', 'Manila Only', 2, 'cebu', 200, 'available', 2300);

-- --------------------------------------------------------

--
-- Table structure for table `flight_information`
--

CREATE TABLE `flight_information` (
  `flight_info_id` int(11) NOT NULL,
  `Flight_code` varchar(255) DEFAULT NULL,
  `arrival_time` varchar(255) DEFAULT NULL,
  `departure_time` int(11) DEFAULT NULL,
  `A_D_status` varchar(255) DEFAULT NULL,
  `flight_status` varchar(255) DEFAULT NULL,
  `delayed_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flight_transaction`
--

CREATE TABLE `flight_transaction` (
  `transaction_id` int(11) NOT NULL,
  `Customer_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_number` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `cabin_type` varchar(255) DEFAULT NULL,
  `luggage_weight` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `flight_code` varchar(255) DEFAULT NULL,
  `airline` varchar(255) DEFAULT NULL,
  `payment_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `flight_transaction`
--

INSERT INTO `flight_transaction` (`transaction_id`, `Customer_id`, `full_name`, `email`, `contact_number`, `price`, `cabin_type`, `luggage_weight`, `quantity`, `flight_code`, `airline`, `payment_status`) VALUES
(1, 111, 'Anthony Nabalde', 'Nabalde@gmail.com', 87898787, 2300, 'economy', 0, 0, '111', 'PAL', 0),
(2, 111, 'James chol', '@gmail.com', 43432432, 2300, 'economy', 45, 2, '111', 'PAL', 0),
(3, 111, 'Jack yu', '@gmail.com', 21321213, 2300, 'economy', 43, 3, '3213', 'PAL', 123),
(4, 0, 'Martin James', '@gmail.com', 0, 2300, 'economy', 0, 0, '', 'PAL', 0),
(5, 0, 'Kyle ', '@gmail.com', 0, 2300, 'premium economy', 0, 0, '', 'PAL', 0),
(6, 0, 'Juan', '@gmail.com', 0, 2300, 'premium economy', 0, 0, '', 'PAL', 0),
(7, 0, 'Aaron James', '@gmail.com', 0, 2300, 'premium economy', 0, 0, '', 'PAL', 0),
(8, 0, 'Carlo', '@gmail.com', 0, 2300, 'business class', 0, 0, '', 'PAL', 0),
(9, 0, 'John', '@gmail.com', 0, 2300, 'business class', 0, 0, '', 'PAL', 0),
(10, 0, 'Johanes', '@gmail.com', 0, 2300, 'first class', 0, 0, '', 'PAL', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_info`
--

CREATE TABLE `ticket_info` (
  `Ticket_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `flight_info_id` int(11) DEFAULT NULL,
  `flight_code` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `seat_position` varchar(255) DEFAULT NULL,
  `departure_time` time DEFAULT NULL,
  `luggage_weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `ticket_info`
--

INSERT INTO `ticket_info` (`Ticket_id`, `transaction_id`, `customer_id`, `flight_info_id`, `flight_code`, `full_name`, `origin`, `destination`, `seat_position`, `departure_time`, `luggage_weight`) VALUES
(3, 2001, 1234, 231, '32132132', 'Anthony', 'Manila only', 'Cebu', '', '00:00:00', 0),
(5, 2002, 143, 231, '32132132', 'James', 'Manila only', 'Cebu', '', '00:00:00', 0),
(6, 2002, 143, 231, '3213', 'Martin', 'Manila only', 'Cebu', '', '00:00:00', 0),
(7, 2002, 143, 231, '3213', 'Martin', 'Manila only', 'Bohol', '', '00:00:00', 0),
(8, 2002, 143, 231, '3213', 'Vince', 'Manila only', 'Bohol', '', '00:00:00', 0),
(9, 200232, 32, 2311221, '3213321', 'marge', 'Manila only', 'Bohol', '', '00:00:00', 0),
(10, 200234, 33, 222, '222', 'Kyle', 'Manila only', 'Bohol', '', '00:00:00', 0),
(11, 20025, 334, 33, '3333', 'Aaron', 'Manila only', 'Bohol', '', '00:00:00', 0),
(12, 20026, 3356, 44, '44444', 'Paul', 'Manila only', 'Bohol', '', '00:00:00', 0),
(13, 26464, 746, 77, '7777', 'Grace', 'Manila only', 'Bohol', '', '00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `User_id` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `email` varchar(277) DEFAULT NULL,
  `is_superuser` tinyint(1) DEFAULT NULL,
  `is_staff` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `date_joined` datetime DEFAULT NULL,
  `Last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`User_id`, `Username`, `Password`, `email`, `is_superuser`, `is_staff`, `is_active`, `date_joined`, `Last_login`) VALUES
(1, '[value-2]', '[value-3]', '[value-4]', 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airline_info`
--
ALTER TABLE `airline_info`
  ADD PRIMARY KEY (`Airline_id`);

--
-- Indexes for table `airplane`
--
ALTER TABLE `airplane`
  ADD PRIMARY KEY (`plane_code`),
  ADD KEY `airline_Id` (`airline_Id`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`Customer_id`),
  ADD KEY `User_id` (`User_id`);

--
-- Indexes for table `flight`
--
ALTER TABLE `flight`
  ADD PRIMARY KEY (`Flight_code`),
  ADD KEY `plane_code` (`plane_code`);

--
-- Indexes for table `flight_information`
--
ALTER TABLE `flight_information`
  ADD PRIMARY KEY (`flight_info_id`),
  ADD KEY `Flight_code` (`Flight_code`);

--
-- Indexes for table `flight_transaction`
--
ALTER TABLE `flight_transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `Customer_id` (`Customer_id`);

--
-- Indexes for table `ticket_info`
--
ALTER TABLE `ticket_info`
  ADD PRIMARY KEY (`Ticket_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `flight_info_id` (`flight_info_id`),
  ADD KEY `flight_code` (`flight_code`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`User_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airline_info`
--
ALTER TABLE `airline_info`
  MODIFY `Airline_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `airplane`
--
ALTER TABLE `airplane`
  MODIFY `plane_code` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `Customer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flight_information`
--
ALTER TABLE `flight_information`
  MODIFY `flight_info_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flight_transaction`
--
ALTER TABLE `flight_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ticket_info`
--
ALTER TABLE `ticket_info`
  MODIFY `Ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `User_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `airplane`
--
ALTER TABLE `airplane`
  ADD CONSTRAINT `airplane_ibfk_1` FOREIGN KEY (`airline_Id`) REFERENCES `airline_info` (`airline_Id`);

--
-- Constraints for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD CONSTRAINT `customer_info_ibfk_1` FOREIGN KEY (`User_id`) REFERENCES `user_account` (`User_id`);

--
-- Constraints for table `flight`
--
ALTER TABLE `flight`
  ADD CONSTRAINT `flight_ibfk_1` FOREIGN KEY (`plane_code`) REFERENCES `airplane` (`plane_code`);

--
-- Constraints for table `flight_information`
--
ALTER TABLE `flight_information`
  ADD CONSTRAINT `flight_information_ibfk_1` FOREIGN KEY (`Flight_code`) REFERENCES `flight` (`Flight_code`);

--
-- Constraints for table `flight_transaction`
--
ALTER TABLE `flight_transaction`
  ADD CONSTRAINT `flight_transaction_ibfk_1` FOREIGN KEY (`Customer_id`) REFERENCES `customer_info` (`Customer_id`);

--
-- Constraints for table `ticket_info`
--
ALTER TABLE `ticket_info`
  ADD CONSTRAINT `ticket_info_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `flight_transaction` (`transaction_id`),
  ADD CONSTRAINT `ticket_info_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer_info` (`customer_id`),
  ADD CONSTRAINT `ticket_info_ibfk_3` FOREIGN KEY (`flight_info_id`) REFERENCES `flight_information` (`flight_info_id`),
  ADD CONSTRAINT `ticket_info_ibfk_4` FOREIGN KEY (`flight_code`) REFERENCES `flight` (`flight_code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
